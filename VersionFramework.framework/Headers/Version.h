//
//  VersionFramework.h
//  VersionFramework
//
//  Created by Damien PRACA on 03/10/16.
//  Copyright © 2016 HighConnexion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCNXBase/HCNXBase.h>

//! Project version number for VersionFramework.
FOUNDATION_EXPORT double VersionFrameworkVersionNumber;

//! Project version string for VersionFramework.
FOUNDATION_EXPORT const unsigned char VersionFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VersionFramework/PublicHeader.h>

@interface Version : HCNXBase {
    
    NSInteger mNbOpening;
    BOOL mCheckOpening;
    BOOL mLetMeHandleAlert;
    NSString * mStoreUrl;
    NSInteger mIntervalInSecond;
    BOOL mNoReminder;
    BOOL mCheckTime;
    BOOL mCheckWhereWeCameFrom;

    BOOL mCheckVersion;
    UIAlertController * alert;
}

/*!
 @brief Use this method to get the instance of Version
 */
+ (Version *)sharedInstance;


- (void)checkVersion:(void (^)(NSDictionary * versionInfo, NSError * error))onCompletion;
- (void)setNumberOfOpeningBeforeDisplayMaj:(NSInteger)nbOpening;
- (void)setTimeIntervalBeforeDisplayMajInSeconds:(NSInteger)nbSeconds;

@end
